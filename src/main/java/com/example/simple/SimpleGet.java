package com.example.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@SpringBootApplication
public class SimpleGet {

	public static void main(String[] args) {
		SpringApplication.run(SimpleGet.class, "--server.port=8082");
	}

    @GetMapping("/simple")
    public Mono<ResponseEntity> simpleGet(){
        return Mono.just("This is simple endpoint")
                .map(ResponseEntity::ok);
    }

}
