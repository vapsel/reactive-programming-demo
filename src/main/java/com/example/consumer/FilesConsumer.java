package com.example.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@SpringBootApplication
public class FilesConsumer {

	public static void main(String[] args) {
		SpringApplication.run(FilesConsumer.class, "--server.port=8081");
	}

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/run")
    public Mono<ResponseEntity> run(){
        WebClient.create()
                .get().uri("http://localhost:8080/files2")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve()
                .bodyToFlux(FileWrapper.class)
                .subscribe(file -> logger.info(file.toString()),
                        e -> logger.error("Error: {}", e),
                        () -> logger.info("Completed"));
        return Mono.just(ResponseEntity.ok().build());
    }



    // todo show memory loading in debug

}
