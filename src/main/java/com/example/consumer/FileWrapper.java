package com.example.consumer;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FileWrapper {

    private String name;
    private Long size;
}

