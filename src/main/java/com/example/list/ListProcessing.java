package com.example.list;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

@RestController
@SpringBootApplication
public class ListProcessing {

    public List<Integer> mock = List.of(1,2,3,4,5,6,7,8,9,10);

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(ListProcessing.class, "--server.port=8083");
    }

    // make it reactive
    @GetMapping(value = "/list", produces = "application/json")
    public Flux<Integer> simpleList(){
        return Flux.range(0, 10)
                .delayElements(Duration.of(200, ChronoUnit.MILLIS));
    }

    @GetMapping(value = "/streamList", produces = "application/stream+json")
    public Flux<Integer> streamList(){
        return Flux.fromIterable(mock)
                .delayElements(Duration.of(200, ChronoUnit.MILLIS))
                .doOnNext(i -> logger.info("Element: " + i))
                .doOnComplete(() -> streamList().subscribe());
    }
}

