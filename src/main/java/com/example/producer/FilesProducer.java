package com.example.producer;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@RestController
@SpringBootApplication
public class FilesProducer {

	public static void main(String[] args) {
		SpringApplication.run(FilesProducer.class, "--server.port=8080");
	}

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Create folder as it is relative path
	 */
	private final static String UPLOAD_DIR = "upload_dir";

	// test performance with siege
	@GetMapping(value = "/files", produces = "application/stream+json")
	public Flux<FileWrapper> getFiles() {
		try {
			return Flux.fromIterable(Files.newDirectoryStream(Paths.get(UPLOAD_DIR)))
					// .delayElements(Duration.of(500, ChronoUnit.MILLIS))
					.map(path -> {
						try {
							TimeUnit.MILLISECONDS.sleep(500L);
						}
						catch (InterruptedException e) {
							e.printStackTrace();
						}
						logger.info("Reactive processing");
						FileWrapper fw = new FileWrapper(path.getFileName().toString(), path.toFile().length());
						return fw;

					})
					// Move blocking operation into another thread
					.subscribeOn(Schedulers.elastic());
		}
		catch (IOException e) {
			return Flux.empty();
		}
	}

	@GetMapping(value = "/fblock", produces = "application/json")
	public List<FileWrapper> getFilesBlocking() throws IOException {
		return StreamSupport.stream(Files.newDirectoryStream(Paths.get(UPLOAD_DIR)).spliterator(), true)
				.map(path -> {
					try {
						TimeUnit.MILLISECONDS.sleep(500L);
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
					logger.info("Stream processing");
					FileWrapper fw = new FileWrapper(path.getFileName().toString(), path.toFile().length());
					return fw;
				}).collect(Collectors.toList());
	}

	@Bean
	public RouterFunction routerFunction() {
		return RouterFunctions.route(GET("/files2"), serverRequest -> {
			logger.info("Subscribing for GET /files2");
			return ok()
					.contentType(MediaType.APPLICATION_STREAM_JSON)
					.body(getFiles(), FileWrapper.class);
		});
	}

	@Data
	@AllArgsConstructor
	public class FileWrapper {

		private String name;
		private Long size;
	}

}
