package com.example.producer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FilesProducerApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void testFlux() {
        Flux.just("Vadym", "Kamil", "Krzysiek", "Paweł")
                .subscribe(System.out::println);

        List<String> list = List.of("Vadym", "Kamil", "Krzysiek");
        Flux.fromIterable(list)
                .subscribe(System.out::println);

        Flux.fromStream(list.stream())
                .subscribe(System.out::println);

    }

    @Test
    public void testMono(){
        Mono.just("1")
                .subscribe(System.out::println);
    }

    @Test
    public void errorTest(){
        Flux.range(-1, 3)
                .map(i -> 10 / i)
                .doOnError(e -> System.out.println("Err:" + e.getMessage()))
                .subscribe(System.out::println, throwable -> System.out.println("Error:" + throwable.getMessage()), () -> System.out.println("Complete"));
    }

}
